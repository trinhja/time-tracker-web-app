﻿(function () {
    'use strict';

    angular
        .module('app', ['ngRoute', 'ngCookies'])
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                controller: 'HomeController',
                templateUrl: 'home/home.view.html',
                controllerAs: 'vm'
            })

            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'login/login.view.html',
                controllerAs: 'vm'
            })

            .when('/register', {
                controller: 'RegisterController',
                templateUrl: 'register/register.view.html',
                controllerAs: 'vm'
            })

            .when('/contact', {
                controller: 'ContactController',
                templateUrl: 'contact/contact.view.html',
                controllerAs: 'vm'
            })

            .when('/clients', {
                controller: 'ClientsController',
                templateUrl: 'clients/clients.view.html',
                controllerAs: 'vm'
            })

            .when('/clients/newclient', {
                controller: 'NewClientController',
                templateUrl: 'clients/newClient/newclient.view.html',
                controllerAs: 'vm'
            })

            .when('/payperiod', {
                controller: 'PayPeriodController',
                templateUrl: 'pay-period/payperiod.view.html',
                controllerAs: 'vm'
            })

            .when('/payperiod/addentry', {
                controller: 'AddEntryController',
                templateUrl: 'pay-period/add-entry/addentry.view.html',
                controllerAs: 'vm'
            })
            .otherwise({ redirectTo: '/login' });
    }

    run.$inject = ['$rootScope', '$location', '$cookies', '$http'];
    function run($rootScope, $location, $cookies, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookies.getObject('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login', '/register', '/contact']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        });
    }

})();