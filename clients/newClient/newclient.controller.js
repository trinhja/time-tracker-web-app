(function () {
    'use strict';

    angular
        .module('app')
        .controller('NewClientController', NewClientController);

    NewClientController.$inject = ['UserService', '$rootScope', '$location', '$scope', '$http'];
    function NewClientController(UserService, $rootScope, $location, $scope, $http) {
        var vm = this;
        vm.user = null;


        vm.addClient = addClient;

        function addClient() {
            vm.dataLoading = true;
            var data = JSON.stringify(vm.client);
            var url = "http://localhost:9091/spring/clientList/body"
            
            $http({
                method: 'POST',
                url: url,
                    headers: {
                        'Content-Type': 'application/json'
                    },
                 data: data
              })
              .then(function(response) {
                $location.path('/clients');
              }, function (error) {
                errorCallback(error.data);
              });
        }

        
        initController();
        

        function initController() {
            loadCurrentUser();
            $scope.username = $rootScope.globals.currentUser.username
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user;
                });
        }
        
 
      
    }

})();