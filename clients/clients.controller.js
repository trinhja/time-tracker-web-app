(function () {
    'use strict';

    angular
        .module('app')
        .controller('ClientsController', ClientsController);

    ClientsController.$inject = ['UserService', '$rootScope', '$http'];
    function ClientsController(UserService, $rootScope, $http) {

        var localhost = 'http://localhost:9091/spring/clientList/';      

        var vm = this;
        vm.user = null;
        vm.allClients = [];
        vm.deleteClient = deleteClient;
        vm.searchClient = searchClient;
        vm.clearSearch = displayClients;
        initController();

        function initController() {
            displayClients();
            loadCurrentUser();
        }

        function displayClients() {
            $http({
                method: 'GET',
                url: localhost + 'get/username/' + $rootScope.globals.currentUser.username
              })
              .then(function(response) {
                vm.allClients = response.data;
              }, function (error) {
                errorCallback(error.data);
              });
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user;
                });
        }

        function deleteClient(id) {
            $http({
                method: 'DELETE',
                url: localhost + 'delete/' + id
              })
              .then(function(response) {
                displayClients()
              }, function (error) {
                errorCallback(error.data);
              });
        }

        function searchClient(){
            $http({
                method: 'GET',
                url: localhost + 'get/search/' + $rootScope.globals.currentUser.username + '/' + vm.client.search
              })
              .then(function(response) {
                vm.allClients = response.data;
              }, function (error) {
                errorCallback(error.data);
              });

        }
    }
})();