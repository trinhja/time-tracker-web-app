(function () {
    'use strict';

    angular
        .module('app')
        .controller('PayPeriodController', PayPeriodController);

    PayPeriodController.$inject = ['UserService', '$rootScope'];
    function PayPeriodController(UserService, $rootScope) {
        var vm = this;

        vm.user = null;
        vm.allUsers = [];

        initController();

        function initController() {
            loadCurrentUser();
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user;
                });
        }

      
    }

})();