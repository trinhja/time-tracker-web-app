(function () {
    'use strict';

    angular
        .module('app')
        .controller('AddEntryController', AddEntryController);

    AddEntryController.$inject = ['UserService', '$rootScope', '$scope', '$location', '$http'];
    function AddEntryController(UserService, $rootScope, $scope, $location, $http) {

        var localhost = 'http://localhost:9091/spring/';

        initController();
        var vm = this;
        vm.user = null;
        vm.addPay = addPay;
        vm.clients = null;
        vm.dateChanged = dateChanged;
        vm.companyPay = companyPay;
        vm.payRate = null;
        
        function initController() {
            $scope.duration = "0h 0m";
            $scope.break = 0;
            $scope.username = $rootScope.globals.currentUser.username
            loadCurrentUser();
            loadClients();
           
           
        }

        // Used query specific company to get the company pay
        function companyPay() {
            $http({
                method: 'GET',
                url: localhost + 'clientList/get/' + vm.pay.client      
              })
              .then(function(response) {
                vm.payRate = response.data;
                console.log(vm.payRate);
                $scope.payRate = vm.payRate.rate;
              }, function (error) {
                errorCallback(error.data);
              });
        }

        function loadCurrentUser() {
            UserService.GetByUsername($rootScope.globals.currentUser.username)
                .then(function (user) {
                    vm.user = user;
                });
        }

        // Function used to loads all clients available for the user
        function loadClients() {
            $http({
                method: 'GET',
                url: localhost + 'clientList/get/username/' + $rootScope.globals.currentUser.username
                
              })
              .then(function(response) {
                vm.clients = response.data;
                console.log(vm.clients);
              }, function (error) {
                errorCallback(error.data);
              });
              
        }



        function dateChanged() {
            
            var seconds = 6000
            var minute = seconds * 10
            var difference = ((vm.pay.endDate - vm.pay.startDate) / minute) - vm.pay.break
            var hoursWorked =  Math.trunc((difference) / 60)
            var minutesWorked = (difference % 60)
            var duration = hoursWorked + "h " + minutesWorked + "m"
            
            if(!isNaN(difference)) {
                $scope.duration = duration;
            } else {
                $scope.duration = "0h 0m";
            }
        }
       


        function addPay() {
            console.log('asd');
            vm.dataLoading = true;
            alert(JSON.stringify(vm.pay))
            
            // var data = vm.client
            // var url = "http://localhost:8080/#!/clients/newclient"
            // $http({
            //     method: 'POST',
            //     url: url,
            //     data: JSON.stringify(data)
            //   })
            //   .then(function (success) {
            //     callback(success);
            //   }, function (error) {
            //     errorCallback(error.data);
            //   });
            $location.path('/payperiod');
        }
    }

    


})();